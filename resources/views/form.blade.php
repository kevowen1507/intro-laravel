<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label for="nama">First name:</label>
        <br><br>
        <input type="text" id="nama"name="first">
        <br><br>
        <label for="nama2">Last name:</label>
        <br>
        <input type="text" id="nama2" name="last"><br><br>
        <label for="gender">Gender:</label>
        <br>
        <input type="radio" name="gen">Male <br>
        <input type="radio" name="gen">Female <br>
        <input type="radio" name="gen">Other <br><br>
        <label for="negara">Nationality:</label><br><br>
        <select name="nation" id="negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Jepang">Jepang</option>
        </select><br><br>
        <label for="bahasa">Language Spoken</label><br><br>
        <input type="checkbox" id="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" id="bahasa">English <br>
        <input type="checkbox" id="bahasa">Other <br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea>
        <br><br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>