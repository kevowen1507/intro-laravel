<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcame');
// });

Route::get('/',"controllerhome@home");
Route::get("/register","authcontroller@form");
Route::post("/welcome","authcontroller@welcome");
Route::post("/kirim","authcontroller@kirim");