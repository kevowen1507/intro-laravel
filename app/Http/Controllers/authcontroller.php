<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class authcontroller extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function form ()
    {
        return view('form');
    }
    public function welcome()
    {
        return view('user');
    }
    public function kirim(Request $request)
    {
        $namadepan = $request["first"];
        $namabelakang = $request["last"];

        return view('user',compact("namadepan","namabelakang"));
    }
}